const jwt = require('jsonwebtoken');
const jwtsecret = require('./../../jwtsecret.json')
const disableAuthorization = true;
const mwRouter = (req, res, next) => {
    console.log('Time:', Date.now(),req.body, req.headers)
    const authorization = req.headers['authorization'];
    console.log(req.path)
    if(disableAuthorization){
      next()
    }
    else if(req.path == '/authenticate/login'){
      next()
    }
    else if(authorization!==undefined){
      jwt.verify(authorization.split(" ")[1],jwtsecret['secretkey'],(err,authdata)=>{
        if(err){
          console.log(err)
          res.status(401).send("Unauthorized");
        }else{
          console.log(authdata);
          next();
        }
      })
    }else{
          res.status(404).send("Not found");
    }
  }


module.exports = {
    mwRouter
}