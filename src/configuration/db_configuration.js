const pg = require('pg');
const redis = require('redis');

const db_connection = async (req, res, next) => {

    const connection_string = process.env.connection_string || "postgres://facento:1ourKPny7pla@ep-sparkling-cherry-693199.us-west-2.aws.neon.tech/facento";
    const pg_client =  new pg.Client({
        connectionString: connection_string,
        ssl: require,
    });
    await pg_client.connect(async err => {
        if (err) {
            console.log('Unable to connect with postgres database', err);
        }
        else {
            console.log('Connected to the postgres database...');
            next();
        }
    });


    // const redis_client = redis.createClient();
    // redis_client.on('error', async (err) => {
    //     console.log('Unable to connect with redis database...', err);
    // });
    // await redis_client.connect(err => {
    //     if (err) {
    //         console.log('Unable to connect with redis database. Error occured at connect...', err);
    //     }
    //     else {
    //         console.log('Connected to the redis database...');
    //         next();
    //     }
    // });
}

module.exports = db_connection;


