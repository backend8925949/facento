const { Model, DataTypes } = require("sequelize");
const {sequelize} = require('./helper.sequelize');


const User = sequelize.define("user", {
    name: DataTypes.TEXT,
    favoriteColor: {
      type: DataTypes.TEXT,
      defaultValue: 'green'
    },
    age: DataTypes.INTEGER,
    cash: DataTypes.INTEGER
  });


module.exports = {
    User
}