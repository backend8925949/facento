const {config} = require( './config')
const axios = require( 'axios')
const instance = (config) => {
  // console.log('axios config --->',config);
  let axios_instance = axios.create(config);
  axios_instance.interceptors.request.use(ic1, ic1_err);
  return axios_instance;

}

let ic1 = (config) => {
  // console.log('intereceptor ------->', config)
  return config;
}

let ic1_err = (error) => {
  // console.log('error ---------->',error)
  return Promise.reject(error);
};

const axios_instance = instance(config);

module.exports={
  axios_instance
}