const { Sequelize, DataTypes } = require('sequelize');
// add database
const sequelize = new Sequelize('dbname', 'username', 'password', {
    host:'localhost',
    dialect: 'mssql'
  });

module.exports = {
    sequelize
}