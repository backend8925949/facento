const ERROR_CODES = {
    FC00010 : {
        code: FC00010,
        message: {
            'en' : 'The specified parameter is invalid.',
            'fr': 'This should be in french!'
        }
    },
    FC00011 : {
        code: FC00011,
        message: {
            'en' : 'The specified parameter is invalid.',
            'fr': 'This should be in french!'
        }
    }
}

module.exports = {
    ERROR_CODES
}