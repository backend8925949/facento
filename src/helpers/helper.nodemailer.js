const nodemailer = require('nodemailer');
const { download_file } = require('./helper.s2s-file-loader');
const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  auth: {
    user: 'email@gmail.com',
    pass: 'some-app-password',
  },
});
let IsServerWindows = true;
const GmailMailer_Verify = ()=>{
    transporter.verify().then(console.log).catch(console.error);
}
const GmailMailer_SendMail = async (req,res,next)=>{
    let {to,cc,bcc, subject, message, file_details_list} = req.body;
    let attachments_paths = await Promise.all(file_details_list.map(async (item, index) => {
        return { path: item.FILE_PATH };
    }))
    let attachments = [];

    attachments_paths.forEach(async(item, index) => {
        let filename = '';
        if(!IsServerWindows) filename = item.path.split('/').pop().split('.')[0] + index;
        else filename = item.path.split('\\')[item.path.split('\\').length - 1].split('.')[0];
        attachments.push({ path: '0-files/' + filename + ".pdf" })
        console.log('Downloading ' + filename, attachments);
        setTimeout(async() => {
           await download_file(item.path, '0-files/' + filename + ".pdf", function () { console.log('Finished Downloading ' + filename) });
        }, 1000 * index);
    })



    if(true)
        setTimeout(() => {
            transporter.sendMail({
                from: 'email@gmail.com, email2@gmail.com', // sender address
                to: to, // list of receivers
                cc: cc,
                bcc: bcc,
                subject: subject, // Subject line
                text: `
                ${message}
                `,
                attachments: attachments
            }).then(info => {
                console.log({info});
                res.send('mail sent.');
            }).catch(console.error);
        }, (file_details_list.length) * 700);
}


module.exports = {
    GmailMailer_Verify,
    GmailMailer_SendMail
}