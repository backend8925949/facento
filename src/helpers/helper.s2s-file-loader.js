const http = require('http'); // or 'https' for https:// URLs
const fs = require('fs');
var request = require('request');

var download_file = async function (file_path, dest, callback) {

   await request.get("download-path" + file_path)
        .on('error', function (err) { console.log(err) })
        .pipe(fs.createWriteStream(dest))
        .on('close', callback);
};

const wait = waitTime => new Promise(resolve => setTimeout(resolve, waitTime));

module.exports = {
    download_file,
    wait,
}