const uuid = require('uuid');
const encrypt = require('crypto');
const nodemailer = require('nodemailer');
const moment = require('moment')

const algorithm = 'aes-256-cbc';
const key = "edc48c6e115ee0fff30370b70e2bde7f5b1a343218f801127ad1b448f8ab67d0";
const iv = "63a1b01226417b03070ace8ecec47439";

const GenerateUuid = async () => {
    const Guid = uuid.v4().toLowerCase();
    return Guid;
}

const GenerateRandomNumber = async (length = Number) => {
    const data = Math.floor(Math.pow(10, length - 1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1)).toString();
    return data;
}

const EncryptHash = async (data = String) => {
    const secretekey = Buffer.from(key, 'hex');
    const initvector = Buffer.from(iv, 'hex');
    const cipher = encrypt.createCipheriv(algorithm, Buffer.from(secretekey), initvector);
    const encrypted = cipher.update(data);
    const encrypteddata = Buffer.concat([encrypted, cipher.final()]);
    return data = encrypteddata.toString('hex')
}

const DecryptHash = async (data = String) => {
    const secretekey = Buffer.from(key, 'hex');
    const initvector = Buffer.from(iv, 'hex');
    const encryptedData = Buffer.from(data, 'hex');
    const decipher = encrypt.createDecipheriv(algorithm, Buffer.from(secretekey), initvector);
    const decrypted = decipher.update(encryptedData);
    const decrypteddata = Buffer.concat([decrypted, decipher.final()]);
    return decrypteddata.toString();

}

const SendEmails = async (fromEmailAddress, toEmailAddress, subject, mailConetent) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: 'omkar@thankucash.com',
            pass: 'Halcyon1!'
        }
    });

    const mailOptions = {
        from: fromEmailAddress,
        to: toEmailAddress,
        subject: subject,
        text: mailConetent
    }

    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            return err.message;
        }
        else {
            return { 'Email sent to : ': info.response }
        }
    });
}

const GetDateTime = async () => {
    let date = moment(new Date()).format('YYYY-MM-DD hh:mm:ss Z');
    return date;
}

const EncodeText = async (data = String) => {
    const bufferText = Buffer.from(data, 'utf-8');
    const base64EncodedString = bufferText.toString('base64');
    return data = base64EncodedString;
}

const DecodeText = async (data = String) => {
    const bufferText = Buffer.from(data, 'base64');
    const base64DecodedString = bufferText.toString('utf-8');
    return data = base64DecodedString;
}

const GenerateSystemName = async (Name = String) => {
    return Name = Name.prototype.replace(' ', '').toLowerCase();
}

const GetListResponse = async (TotalRecords = Number, Offset = Number, Limit = Number, Data = Object, response = ohelper.ListResponse) => {
    response.TotalRecords = TotalRecords;
    response.Data = Data;
    response.Offset = Offset;
    response.Limit = Limit;
    return response;
}

const GetSortCondition = async (FieldName = String, SortOrder) => {
    const SortExpression = "";
    if (FieldName) {
        if (SortOrder) {
            SortExpression = (FieldName + ":" + SortOrder);
        }
    }
    return SortExpression;
}

const GetSearchCondition = async (SearchColumnName = String, SearchColumnValue = String) => {
    const SearchCondition = "";
    if (SearchColumnName) {
        SearchCondition = (SearchColumnName + ":" + SearchColumnValue);
    }
    return SearchCondition;
}

module.exports = {
    GenerateUuid,
    GenerateRandomNumber,
    EncryptHash,
    DecryptHash,
    SendEmails,
    GetDateTime,
    EncodeText,
    DecodeText,
    GenerateSystemName,
    GetListResponse,
    GetSortCondition,
    GetSearchCondition,
}