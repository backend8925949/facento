const formatJavascriptDateToMsSQLDateTime = (date)=>{
    
    return date!=null?new Date(date).toISOString().slice(0, 23).replace('T', ' ').replace('Z',''):date;
}

module.exports={
    formatJavascriptDateToMsSQLDateTime
}