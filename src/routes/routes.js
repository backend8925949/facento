const express = require('express');
const { mwRouter } = require('../middlewares/mw.router');
const db = require('./../configuration/db_configuration')
const router = express.Router();

//******************** middleware functions ***************************/
router.use(db);
router.use(mwRouter);
//***************************************************************/

//******************** authentication ***************************/
const auth_router = require('./../modules/authentication/auth.route');
router.use('/authenticate',auth_router);
//***************************************************************/

//******************** user routes ***************************/
const user_router =  require('./../modules/core/user/user.route');
router.use('/user',user_router);
//***************************************************************/

//******************** organization routes ***************************/
const organization_router =  require('./../modules/core/organization/organization.route');
router.use('/organization', organization_router);
//***************************************************************/

//******************** location routes ***************************/
const location_router =  require('./../modules/master/location/location.route');
router.use('/location', location_router);
//***************************************************************/

//******************** currency routes ***************************/
const currency_router =  require('./../modules/master/currency/currency.route');
router.use('/currency', currency_router);
//***************************************************************/

//******************** timezone routes ***************************/
const timezone_router =  require('./../modules/master/timezone/timezone.route');
router.use('/timezone', timezone_router);
//***************************************************************/

//******************** address routes ***************************/
const address_router =  require('./../modules/core/address/address.route');
router.use('/address', address_router);
//***************************************************************/

//******************** contac_person routes ***************************/
const contac_person_router =  require('./../modules/core/contact_person/contact_person.route');
router.use('/contact', contac_person_router);
//***************************************************************/

//******************** storage routes ***************************/
const storage_router =  require('./../modules/core/storage/storage.route');
router.use('/storage', storage_router);
//***************************************************************/

//******************** working_hours routes ***************************/
const working_hours_router =  require('./../modules/core/working_hours/working_hours.route');
router.use('/work', working_hours_router);
//***************************************************************/

//******************** account_tax_authority routes ***************************/
const account_tax_authority_router =  require('./../modules/account_tax/acc_tax_authority/acc_tax_authority.route');
router.use('/acctax/authority', account_tax_authority_router);
//***************************************************************/

//******************** account_tax routes ***************************/
const account_tax_router =  require('./../modules/account_tax/acc_tax/acc_tax.route');
router.use('/acctax', account_tax_router);
//***************************************************************/

//******************** account_tax_group routes ***************************/
const account_tax_group_router =  require('./../modules/account_tax/acc_tax_group/acc_tax_group.route');
router.use('/acctax/group', account_tax_group_router);
//***************************************************************/

//******************** account_tax_group_map routes ***************************/
const account_tax_group_map_router =  require('./../modules/account_tax/acc_tax_group_map/acc_tax_group_map.route');
router.use('/acctax/group/map', account_tax_group_map_router);
//***************************************************************/

module.exports = router;