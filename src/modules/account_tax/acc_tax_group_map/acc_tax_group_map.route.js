const express = require('express');
const router = express.Router();

const acc_tax_group_map_controller = require('./acc_tax_group_map.controller');

router.post('/saveacctaxgroupmap', acc_tax_group_map_controller.save_acc_tax_group_map);
router.put('/updateacctaxgroupmap', acc_tax_group_map_controller.update_acc_tax_group_map);
router.delete('/deleteacctaxgroupmap', acc_tax_group_map_controller.delete_acc_tax_group_map);

/**
 * @swagger
 * /api/v1/acctax/group/map/getacctaxgroup/{tax_id}{tax_group_id}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the details of a single account tax group map.
 *     description: Retrieves the details of a single account tax group map. Can be used to populate the details of the account tax group map.
 *     parameters:
 *       - in: path
 *         name: tax_id
 *         required: true
 *         description: String uuid of the account tax.
 *         schema:
 *           type: string
 *       - in: path
 *         name: tax_group_id
 *         required: true
 *         description: String uuid of the account tax group.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getacctaxgroupmap/:tax_id/:tax_group_id', acc_tax_group_map_controller.get_acc_tax_group_map_details);

/**
 * @swagger
 * /api/v1/acctax/group/map/getacctaxgroupmaps/{limit}{offset}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the list of the account tax group maps.
 *     description: Retrieves the list of the account tax group maps.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the account tax group maps.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the account tax group maps.
 *         schema:
 *           type: integer
 */
router.get('/getacctaxgroupmaps/:limit/:offset', acc_tax_group_map_controller.get_acc_tax_group_maps);

module.exports = router