const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_acc_tax_group_map = async (req, res, next) => {
    try {
        let acc_tax_group_map_details = await axios_instance.post('/api/rest/acc/taxgrpmap/saveacctaxgroupmap', { tax_id: req.body.tax_id , tax_group_id: req.body.tax_group_id, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let acc_tax_group_map = acc_tax_group_map_details.data.insert_facento_acc_tax_group_map.returning;
        if (acc_tax_group_map) {
            return res.status(200).send({ acc_tax_group_map: acc_tax_group_map[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create acc_tax_group_map" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_acc_tax_group_map = async (req, res, next) => {
    try {
        let acc_tax_group_map_details = await axios_instance.put('/api/rest/acc/taxgrpmap/updateacctaxgroupmap', { tax_id: req.body.tax_id , tax_group_id: req.body.tax_group_id, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let acc_tax_group_map = acc_tax_group_map_details.data.update_facento_acc_tax_group_map.returning;
        if (acc_tax_group_map) {
            return res.status(200).send({ acc_tax_group_map: acc_tax_group_map[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid acc_tax_group_map details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax_group_maps = async (req, res, next) => {
    try {
        let acc_tax_group_map_records = await axios_instance.post('/api/rest/acc/taxgrpmap/getacctaxgroupmaps', { limit: req.params.limit, offset: req.params.offset });
        let acc_tax_group_maps = acc_tax_group_map_records.data.facento_acc_tax_group_map;
        if (acc_tax_group_maps) {
            return res.status(200).send({ records: acc_tax_group_maps });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_acc_tax_group_map_details = async (req, res, next) => {
    try {
        let acc_tax_group_map_details = await axios_instance.post('/api/rest/acc/taxgrpmap/getacctaxgroupmap', { tax_id: req.params.tax_id , tax_group_id: req.params.tax_group_id });
        let acc_tax_group_map = acc_tax_group_map_details.data.facento_acc_tax_group_map;
        if (acc_tax_group_map) {
            return res.status(200).send({ data: acc_tax_group_map });
        }
        else {
            return res.status(404).send({ message: "acc_tax_group_map details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_acc_tax_group_map = async (req, res, next) => {
    try {
        const acc_tax_group_map_details = await get_acc_tax_group_map(req.params.tax_id, req.params.tax_group_id);
        console.log('acc_tax_group_map....', acc_tax_group_map_details);
        if (acc_tax_group_map_details) {
            let delete_acc_tax_group_map = await axios_instance.delete('/api/rest/acc/taxgrpmap/deleteacctaxgroupmap', { tax_id: acc_tax_group_map_details.tax_id , tax_group_id: acc_tax_group_map_details.tax_group_id });
            if (delete_acc_tax_group_map) {
                return res.status(200).send({ message: 'acc_tax_group_map details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "acc_tax_group_map details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax_group_map = async (tax_id, tax_group_id) => {
    try {
        let acc_tax_group_map_details = await axios_instance.post('/api/rest/acc/taxgrpmap/getacctaxgroupmap', { tax_id: tax_id , tax_group_id: tax_group_id });
        let acc_tax_group_map = acc_tax_group_map_details.data.facento_acc_tax_group_map;
        if (acc_tax_group_map) {
            return acc_tax_group_map[0];
        }
        else {
            return { message: "acc_tax_group_map details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_acc_tax_group_map,
    update_acc_tax_group_map,
    delete_acc_tax_group_map,
    get_acc_tax_group_maps,
    get_acc_tax_group_map_details,
}