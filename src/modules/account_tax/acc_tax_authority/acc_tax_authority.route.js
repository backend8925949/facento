const express = require('express');
const router = express.Router();


const acc_tax_authority_controller = require('./acc_tax_authority.controller');

router.post('/saveacctaxauthority', acc_tax_authority_controller.save_acc_tax_authority);
router.put('/updateacctaxauthority', acc_tax_authority_controller.update_acc_tax_authority);
router.delete('/deleteacctaxauthority', acc_tax_authority_controller.delete_acc_tax_authority);

/**
 * @swagger
 * /api/v1/acctax/authority/getacctaxauthority/{authority_id}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the details of a single account tax.
 *     description: Retrieves the details of a single account tax. Can be used to populate the details of the account tax authority for particular account.
 *     parameters:
 *       - in: path
 *         name: authority_id
 *         required: true
 *         description: String uuid of the account tax authority to retrieve.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getacctaxauthority/:authority_id', acc_tax_authority_controller.get_acc_tax_authority_details);

/**
 * @swagger
 * /api/v1/acctax/authority/getacctaxauthorities/{limit}{offset}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the list of the account tax authorities.
 *     description: Retrieves the list of the account tax authorities.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the account tax authorities.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the account tax authorities.
 *         schema:
 *           type: integer
 */
router.get('/getacctaxauthorities/:limit/:offset', acc_tax_authority_controller.get_acc_tax_authoritys);

module.exports = router