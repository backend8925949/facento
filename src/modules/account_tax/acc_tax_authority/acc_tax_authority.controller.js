const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_acc_tax_authority = async (req, res, next) => {
    try {
        let acc_tax_authority_details = await axios_instance.post('/api/rest/acc/authority/saveacctaxauthority', { authority_id: req.body.authority_id, name: req.body.name, registarion_number: req.body.registarion_number, registration_label: req.body.registration_label, description: req.body.description, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let acc_tax_authority = acc_tax_authority_details.data.insert_facento_acc_tax_aauthority.returning;
        if (acc_tax_authority) {
            return res.status(200).send({ acc_tax_authority: acc_tax_authority[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create acc_tax_authority" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_acc_tax_authority = async (req, res, next) => {
    try {
        let acc_tax_authority_details = await axios_instance.put('/api/rest/acc/authority/updateacctaxauthority', { authority_id: req.body.authority_id, name: req.body.name, registarion_number: req.body.registarion_number, registration_label: req.body.registration_label, description: req.body.description, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let acc_tax_authority = acc_tax_authority_details.data.update_facento_acc_tax_aauthority.returning;
        if (acc_tax_authority) {
            return res.status(200).send({ acc_tax_authority: acc_tax_authority[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid acc_tax_authority details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax_authoritys = async (req, res, next) => {
    try {
        let acc_tax_authority_records = await axios_instance.post('/api/rest/acc/authority/getacctaxauthorities', { limit: req.params.limit, offset: req.params.offset });
        let acc_tax_authoritys = acc_tax_authority_records.data.facento_acc_tax_aauthority;
        if (acc_tax_authoritys) {
            return res.status(200).send({ records: acc_tax_authoritys });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_acc_tax_authority_details = async (req, res, next) => {
    try {
        let acc_tax_authority_details = await axios_instance.post('/api/rest/acc/authority/getacctaxauthority', { authority_id: req.params.authority_id });
        let acc_tax_authority = acc_tax_authority_details.data.facento_acc_tax_aauthority;
        if (acc_tax_authority) {
            return res.status(200).send({ data: acc_tax_authority });
        }
        else {
            return res.status(404).send({ message: "acc_tax_authority details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_acc_tax_authority = async (req, res, next) => {
    try {
        const acc_tax_authority_details = await get_acc_tax_authority(req.body.authority_id);
        console.log('acc_tax_authority....', acc_tax_authority_details);
        if (acc_tax_authority_details) {
            let delete_acc_tax_authority = await axios_instance.delete('/api/rest/acc/authority/deleteacctaxauthority', { authority_id: acc_tax_authority_details.uuid });
            if (delete_acc_tax_authority) {
                return res.status(200).send({ message: 'acc_tax_authority details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "acc_tax_authority details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax_authority = async (acc_tax_authority_id) => {
    try {
        let acc_tax_authority_details = await axios_instance.post('/api/rest/acc/authority/getacctaxauthority', { authority_id: authority_id });
        let acc_tax_authority = acc_tax_authority_details.data.facento_acc_tax_aauthority;
        if (acc_tax_authority) {
            return acc_tax_authority[0];
        }
        else {
            return { message: "acc_tax_authority details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_acc_tax_authority,
    update_acc_tax_authority,
    delete_acc_tax_authority,
    get_acc_tax_authoritys,
    get_acc_tax_authority_details,
}