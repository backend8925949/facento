const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_acc_tax_group = async (req, res, next) => {
    try {
        let acc_tax_group_details = await axios_instance.post('/api/rest/acc/taxgrp/saveacctaxgroup', { uuid: await helpers.GenerateUuid(), name: req.body.name, percentage: req.body.percentage, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let acc_tax_group = acc_tax_group_details.data.insert_facento_acc_tax_group.returning;
        if (acc_tax_group) {
            return res.status(200).send({ acc_tax_group: acc_tax_group[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create acc_tax_group" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_acc_tax_group = async (req, res, next) => {
    try {
        let acc_tax_group_details = await axios_instance.put('/api/rest/acc/taxgrp/updateacctaxgroup', { group_id: req.body.group_id, name: req.body.name, percentage: req.body.percentage, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let acc_tax_group = acc_tax_group_details.data.update_facento_acc_tax_group.returning;
        if (acc_tax_group) {
            return res.status(200).send({ acc_tax_group: acc_tax_group[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid acc_tax_group details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax_groups = async (req, res, next) => {
    try {
        let acc_tax_group_records = await axios_instance.post('/api/rest/acc/taxgrp/getacctaxgroups', { limit: req.params.limit, offset: req.params.offset });
        let acc_tax_groups = acc_tax_group_records.data.facento_acc_tax_group;
        if (acc_tax_groups) {
            return res.status(200).send({ records: acc_tax_groups });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_acc_tax_group_details = async (req, res, next) => {
    try {
        let acc_tax_group_details = await axios_instance.post('/api/rest/acc/taxgrp/getacctaxgroup', { user_id: req.params.group_id });
        let acc_tax_group = acc_tax_group_details.data.facento_acc_tax_group;
        if (acc_tax_group) {
            return res.status(200).send({ data: acc_tax_group });
        }
        else {
            return res.status(404).send({ message: "acc_tax_group details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_acc_tax_group = async (req, res, next) => {
    try {
        const acc_tax_group_details = await get_acc_tax_group(req.body.acc_tax_group_id);
        console.log('acc_tax_group....', acc_tax_group_details);
        if (acc_tax_group_details) {
            let delete_acc_tax_group = await axios_instance.delete('/api/rest/acc/taxgrp/deleteacctaxgroup', { group_id: acc_tax_group_details.uuid });
            if (delete_acc_tax_group) {
                return res.status(200).send({ message: 'acc_tax_group details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "acc_tax_group details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax_group = async (group_id) => {
    try {
        let acc_tax_group_details = await axios_instance.post('/api/rest/acc/taxgrp/getacctaxgroup', { group_id: group_id });
        let acc_tax_group = acc_tax_group_details.data.facento_acc_tax_group;
        if (acc_tax_group) {
            return acc_tax_group[0];
        }
        else {
            return { message: "acc_tax_group details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_acc_tax_group,
    update_acc_tax_group,
    delete_acc_tax_group,
    get_acc_tax_groups,
    get_acc_tax_group_details,
}