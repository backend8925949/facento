const express = require('express');
const router = express.Router();


const acc_tax_group_controller = require('./acc_tax_group.controller');

router.post('/saveacctaxgroup', acc_tax_group_controller.save_acc_tax_group);
router.put('/updateacctaxgroup', acc_tax_group_controller.update_acc_tax_group);
router.delete('/deleteacctaxgroup', acc_tax_group_controller.delete_acc_tax_group);

/**
 * @swagger
 * /api/v1/acctax/group/getacctaxgroup/{group_id}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the details of a single account tax group.
 *     description: Retrieves the details of a single account tax group. Can be used to populate the details of the account tax group.
 *     parameters:
 *       - in: path
 *         name: group_id
 *         required: true
 *         description: String uuid of the account tax group to retrieve.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getacctaxgroup/:group_id', acc_tax_group_controller.get_acc_tax_group_details);

/**
 * @swagger
 * /api/v1/acctax/group/getacctaxgroups/{limit}{offset}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the list of the account tax groups.
 *     description: Retrieves the list of the account tax groups.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the account tax groups.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the account tax groups.
 *         schema:
 *           type: integer
 */
router.get('/getacctaxgroups/:limit/:offset', acc_tax_group_controller.get_acc_tax_groups);

module.exports = router