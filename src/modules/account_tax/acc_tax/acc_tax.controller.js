const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_acc_tax = async (req, res, next) => {
    try {
        let acc_tax_details = await axios_instance.post('/api/rest/acc/tax/saveacctax', { uuid: await helpers.GenerateUuid(), is_editable: req.body.is_editable, is_value_added: req.body.is_value_added, name: req.body.name, percentage: req.body.percentage, factor: req.body.factor, country_id: req.body.country_id, specific_type: req.body.specific_type, tax_authority_id: req.body.tax_authority_id, type: req.body.type, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let acc_tax = acc_tax_details.data.insert_facento_acc_tax.returning;
        if (acc_tax) {
            return res.status(200).send({ acc_tax: acc_tax[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create acc_tax" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_acc_tax = async (req, res, next) => {
    try {
        let acc_tax_details = await axios_instance.put('/api/rest/acc/tax/updateacctax', { acc_tax_id: req.body.acc_tax_id, is_editable: req.body.is_editable, is_value_added: req.body.is_value_added, name: req.body.name, percentage: req.body.percentage, factor: req.body.factor, country_id: req.body.country_id, specific_type: req.body.specific_type, tax_authority_id: req.body.tax_authority_id, type: req.body.type, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let acc_tax = acc_tax_details.data.update_facento_acc_tax.returning;
        if (acc_tax) {
            return res.status(200).send({ acc_tax: acc_tax[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid acc_tax details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_taxs = async (req, res, next) => {
    try {
        let acc_tax_records = await axios_instance.post('/api/rest/acc/tax/getacctaxes', { limit: req.params.limit, offset: req.params.offset });
        let acc_taxes = acc_tax_records.data.facento_acc_tax;
        if (acc_taxes) {
            return res.status(200).send({ records: acc_taxes });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_acc_tax_details = async (req, res, next) => {
    try {
        let acc_tax_details = await axios_instance.post('/api/rest/acc/tax/getacctax', { acc_tax_id: req.params.acc_tax_id });
        let acc_tax = acc_tax_details.data.facento_acc_tax;
        if (acc_tax) {
            return res.status(200).send({ data: acc_tax });
        }
        else {
            return res.status(404).send({ message: "acc_tax details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_acc_tax = async (req, res, next) => {
    try {
        const acc_tax_details = await get_acc_tax(req.body.acc_tax_id);
        console.log('acc_tax....', acc_tax_details);
        if (acc_tax_details) {
            let delete_acc_tax = await axios_instance.delete('/api/rest/acc/tax/deleteacctax', { acc_tax_id: acc_tax_details.uuid });
            if (delete_acc_tax) {
                return res.status(200).send({ message: 'acc_tax details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "acc_tax details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_acc_tax = async (acc_tax_id) => {
    try {
        let acc_tax_details = await axios_instance.post('/api/rest/acc/tax/getacctax', { acc_tax_id: acc_tax_id });
        let acc_tax = acc_tax_details.data.facento_acc_tax;
        if (acc_tax) {
            return acc_tax[0];
        }
        else {
            return { message: "acc_tax details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_acc_tax,
    update_acc_tax,
    delete_acc_tax,
    get_acc_taxs,
    get_acc_tax_details,
}