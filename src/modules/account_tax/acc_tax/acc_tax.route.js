const express = require('express');
const router = express.Router();


const acc_tax_controller = require('./acc_tax.controller');

router.post('/saveacctax', acc_tax_controller.save_acc_tax);
router.put('/updateacctax', acc_tax_controller.update_acc_tax);
router.delete('/deleteacc_tax', acc_tax_controller.delete_acc_tax);

/**
 * @swagger
 * /api/v1/acctax/getacctax/{acc_tax_id}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the details of a single account tax.
 *     description: Retrieves the details of a single account tax. Can be used to populate the details of the account tax for particular account.
 *     parameters:
 *       - in: path
 *         name: acc_tax_id
 *         required: true
 *         description: String uuid of the account tax to retrieve.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getacctax/:acc_tax_id', acc_tax_controller.get_acc_tax_details);

/**
 * @swagger
 * /api/v1/acctax/getacctaxes/{limit}{offset}:
 *   get:
 *     tags:
 *       - account_tax
 *     summary: Retrieves the list of the account tax.
 *     description: Retrieves the list of the account tax.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the account tax.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the account tax.
 *         schema:
 *           type: integer
 */
router.get('/getacctaxes/:limit/:offset', acc_tax_controller.get_acc_taxs);

module.exports = router