const express = require('express');
const router = express.Router();

const auth_controller = require('./auth.controller');

router.post('/login', auth_controller.authenticate);

/**
 * @swagger
 * /api/v1/authenticate/userlogin:
 *   post:
 *     tags:
 *       - authentication
 *     summary: This API will authenticate the user.
 *     description: This API will authenticate the user and if authentication is successfull will retirn a token which will be used for the authorization for the other APIs.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 description: The account user's user name or email address.
 *                 example: omkar@gmail.com
 *               password:
 *                 type: string
 *                 description: The account user's password.
 *                 example: Omkar@123
 *     responses:
 *       200:
 *         description: A list of users.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 username:
 *                   token: string
 *                   description: Authentication token.
 *                   example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2ODQxMzI0MDMsImV4cCI6MTY4NDIxNTIwM30.LkEfAe1ew221RptKh94B-HCVUxQi_kf3MLjfiqruEMM"
 */
router.post('/userlogin/:somevalue/:othervalue', auth_controller.user_login);

module.exports = router;