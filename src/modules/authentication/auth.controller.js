const jwtsecret = require("./../../../jwtsecret.json");
const jwt = require('jsonwebtoken');
const user_controller = require('./../core/user/user.controller')

const users = {
     kiran_d: {
          response: {
               USER_NAME: "Kiran",
               USER_TYPE: 1,
               USER_ID: 1
          },
          auth: {
               USER_NAME: "kiran_d",
               PASSWORD: "kiran_d"
          }
     }
}

let secretkey = jwtsecret["secretkey"]

const authenticateOld = async (req, res, next) => {
     let testing = false;
     try {
          console.log(req.body, req.body.username, req.body.username === users[req.body.username].auth.USER_NAME);
          if (req.body.username === users[req.body.username].auth.USER_NAME && req.body.password === users[req.body.username].auth.PASSWORD) {
               res.send(users[req.body.username].response)
          } else {

               res.send(401)
          }
     } catch (error) {
          console.log(error)
          res.send(401)
     }
}

const authenticate = async (req, res, next) => {
     try {
          if (req.body.username === users[req.body.username].auth.USER_NAME && req.body.password === users[req.body.username].auth.PASSWORD) {
               jwt.sign(users[req.body.username].response, secretkey, { expiresIn: '23h' }, (err, token) => {
                    res.send({ token });
               })
          } else {
               res.send(401)
          }
     } catch (err) {
          res.send(500);
     }
}

const user_login = async (req, res, next) => {
     /* 
     #swagger.tags = ['Users']
     #swagger.autoBody=true 
     #swagger.summary = 'This is a user login API'
     #swagger.parameters['somevalue'] = {
          description: 'adding new user.',
          type: "string"
     }
     #swagger.parameters['othervalue'] = {
          description: 'adding new number.',
          type: "number"
     }
     #swagger.requestBody = {
          required: true,
          content: {
               "application/json":{
                    schema:{
                         type:"object",
                         properties:{
                              username:{
                                   type: "string",
                                   description: "I am coder.",
                                   example: "kiran@facento.com"
                              },
                              password:{
                                   type: "Number",
                                   example: 1
                              }
                         }
                    }
               }
          }
     }
     */
     try {
          if (req) {
               let user_details = await user_controller.get_user(req.body.username);
               if (user_details) {
                    console.log("user details....", user_details);
                    let user = await user_controller.get_user_creds(user_details.id, req.body.username, req.body.password);
                    if (user) {
                         console.log("user creds....", user);
                         jwt.sign({user_details: user[0]}, secretkey, { expiresIn: '23h' }, (err, token) => {
                              if (err) {
                                   res.status(401).send({ message: err.message });
                              }
                              else {
                                   res.status(200).send({ token });
                              }
                         });
                    }
                    else {
                         res.status(401).send({ message: 'Unauthorized' })
                    }
               }
               else {
                    res.status(401).send({ message: 'Unauthorized' })
               }
          }
          else {
               res.status(401).send({ message: 'Unauthorized' })
          }
     } catch (err) {
          res.status(500).send({ message: err.message });
     }
}

module.exports = {
     authenticateOld,
     authenticate,
     user_login,
}


//time format: 2023-05-02 11:00:00 +5:30