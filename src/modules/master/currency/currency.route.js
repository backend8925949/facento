const express = require('express');
const router = express.Router();


const currency_controller = require('./currency.controller');

router.post('/registercurrency', currency_controller.save_currency);
router.put('/updatecurrency', currency_controller.update_currency);
router.delete('/deletecurrency', currency_controller.delete_currency);

/**
 * @swagger
 * /api/v1/currency/getcurrency/{currency_id}:
 *   get:
 *     tags:
 *       - master
 *     summary: Retrieves the details of a currency.
 *     description: Retrieves the details of a single currency. Can be used to populate the details of the currency.
 *     parameters:
 *       - in: path
 *         name: currency_id
 *         required: true
 *         description: String uuid of the currency.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getcurrency/:currency_id', currency_controller.get_currency_details);

/**
 * @swagger
 * /api/v1/currency/getcurrencies/{limit}{offset}:
 *   get:
 *     tags:
 *       - master
 *     summary: Retrieves the list of the currencies.
 *     description: Retrieves the list of the currencies.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the currencies.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the currencies.
 *         schema:
 *           type: integer
 */
router.get('/getcurrencies/:limit/:offset', currency_controller.get_currencys);

module.exports = router