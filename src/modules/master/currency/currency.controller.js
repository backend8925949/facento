const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_currency = async (req, res, next) => {
    try {
        let currency_details = await axios_instance.post('/api/rest/currency/savecurrency', { uuid: await helpers.GenerateUuid(), name: req.body.name, symbol: req.body.symbol, format: req.body.format, code: req.body.code, organization_id: req.body.organization_id, precision: req.body.precision, created_by: req.body.created_by, created_date: await helpers.GetDateTime() });
        let currency = currency_details.data.insert_facento_core_currency.returning;
        if (currency) {
            return res.status(200).send({ currency: currency[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create currency" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_currency = async (req, res, next) => {
    try {
        let currency_details = await axios_instance.put('/api/rest/currency/updatecurrency', { currency_id: req.body.currency_id, name: req.body.name, symbol: req.body.symbol, format: req.body.format, code: req.body.code, organization_id: req.body.organization_id, precision: req.body.precision, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let currency = currency_details.data.update_facento_core_currency.returning;
        if (currency) {
            return res.status(200).send({ currency: currency[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid currency details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_currencys = async (req, res, next) => {
    try {
        let currency_records = await axios_instance.post('/api/rest/currency/getcurrencies', { limit: req.params.limit, offset: req.params.offset });
        let currencys = currency_records.data.facento_core_currency;
        if (currencys) {
            return res.status(200).send({ records: currencys });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_currency_details = async (req, res, next) => {
    try {
        let currency_details = await axios_instance.post('/api/rest/currency/getcurrency', { currency_id: req.params.currency_id });
        let currency = currency_details.data.facento_core_currency;
        if (currency) {
            return res.status(200).send({ data: currency });
        }
        else {
            return res.status(404).send({ message: "Currency details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_currency = async (req, res, next) => {
    try {
        const currency_details = await get_currency(req.body.currency_id);
        console.log('currency....', currency_details);
        if (currency_details) {
            let delete_creds = await axios_instance.delete('/api/rest/currency/deletecurrency', { currency_id: currency_details.uuid });
            if (delete_creds) {
                return res.status(200).send({ message: 'Currency details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "Currency details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_currency = async (currency_id) => {
    try {
        let currency_details = await axios_instance.post('/api/rest/currency/getcurrency', { currency_id: currency_id });
        let currency = currency_details.data.facento_core_currency;
        if (currency) {
            return currency[0];
        }
        else {
            return { message: "Currency details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_currency,
    update_currency,
    delete_currency,
    get_currencys,
    get_currency_details,
}