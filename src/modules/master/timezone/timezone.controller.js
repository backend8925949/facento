const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_timezone = async (req, res, next) => {
    try {
        let timezone_details = await axios_instance.post('/api/rest/tmz/savetimezone', { uuid: await helpers.GenerateUuid(), internal_code: req.body.internal_code, offset_dst: req.body.offset_dst, offset_std: req.body.offset_std, time_zone: req.body.time_zone, code_std: req.body.code_std, code_dst: req.body.code_dst, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let timezone = timezone_details.data.insert_facento_core_geo_timezone.returning;
        if (timezone) {
            return res.status(200).send({ timezone: timezone[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create timezone" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_timezone = async (req, res, next) => {
    try {
        let timezone_details = await axios_instance.put('/api/rest/tmz/updatetimezone', { timezone_id: req.body.timezone_id, internal_code: req.body.internal_code, offset_dst: req.body.offset_dst, offset_std: req.body.offset_std, time_zone: req.body.time_zone, code_std: req.body.code_std, code_dst: req.body.code_dst, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let timezone = timezone_details.data.update_facento_core_geo_timezone.returning;
        if (timezone) {
            return res.status(200).send({ timezone: timezone[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid timezone details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_timezones = async (req, res, next) => {
    try {
        let timezone_records = await axios_instance.post('/api/rest/tmz/gettimezones', { limit: req.params.limit, offset: req.params.offset });
        let timezones = timezone_records.data.facento_core_geo_timezone;
        if (timezones) {
            return res.status(200).send({ records: timezones });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_timezone_details = async (req, res, next) => {
    try {
        let timezone_details = await axios_instance.post('/api/rest/tmz/gettimezone', { timezone_id: req.params.timezone_id });
        let timezone = timezone_details.data.facento_core_geo_timezone;
        if (timezone) {
            return res.status(200).send({ data: timezone });
        }
        else {
            return res.status(404).send({ message: "Timezone details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_timezone = async (req, res, next) => {
    try {
        const timezone_details = await get_timezone(req.body.timezone_id);
        console.log('timezone....', timezone_details);
        if (timezone_details) {
            let delete_timezone = await axios_instance.delete('/api/rest/tmz/deletetimezone', { timezone_id: timezone_details.uuid });
            if (delete_timezone) {
                return res.status(200).send({ message: 'Timezone details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "Timezone details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_timezone = async (timezone_id) => {
    try {
        let timezone_details = await axios_instance.post('/api/rest/tmz/gettimezone', { timezone_id: timezone_id });
        let timezone = timezone_details.data.facento_core_geo_timezone;
        if (timezone) {
            return timezone[0];
        }
        else {
            return { message: "Timezone details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_timezone,
    update_timezone,
    delete_timezone,
    get_timezones,
    get_timezone_details,
}