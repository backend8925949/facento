const express = require('express');
const router = express.Router();


const timezone_controller = require('./timezone.controller');

router.post('/registertimezone', timezone_controller.save_timezone);
router.put('/updatetimezone', timezone_controller.update_timezone);
router.delete('/deletetimezone', timezone_controller.delete_timezone);

/**
 * @swagger
 * /api/v1/timezone/gettimezone/{timezone_id}:
 *   get:
 *     tags:
 *       - master
 *     summary: Retrieves the details of a single timezone.
 *     description: Retrieves the details of a single timezone. Can be used to populate the details of the timezone.
 *     parameters:
 *       - in: path
 *         name: timezone_id
 *         required: true
 *         description: String uuid of the timezone.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/gettimezone/:timezone_id', timezone_controller.get_timezone_details);

/**
 * @swagger
 * /timezone/gettimezones/{limit}{offset}:
 *   get:
 *     tags:
 *       - master
 *     summary: Retrieves the list of the timezones.
 *     description: Retrieves the list of the timezones.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the timezones.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the timezones.
 *         schema:
 *           type: integer
 */
router.get('/gettimezones/:limit/:offset', timezone_controller.get_timezones);

module.exports = router