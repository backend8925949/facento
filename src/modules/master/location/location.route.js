const express = require('express');
const router = express.Router();


const location_controller = require('./location.controller');

router.post('/registerlocation', location_controller.save_location);
router.put('/updatelocation', location_controller.update_location);
router.delete('/deletelocation', location_controller.delete_location);

/**
 * @swagger
 * /api/v1/location/getlocation/{location_id}:
 *   get:
 *     tags:
 *       - master
 *     summary: Retrieves the details of a single geo location.
 *     description: Retrieves the details of a single geo location. Can be used to populate the details of the geo location.
 *     parameters:
 *       - in: path
 *         name: address_id
 *         required: true
 *         description: String uuid of the geo location.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getlocation/:location_id', location_controller.get_location_details);

/**
 * @swagger
 * /api/v1/location/getlocations/{limit}{offset}:
 *   get:
 *     tags:
 *       - master
 *     summary: Retrieves the list of the locations.
 *     description: Retrieves the list of the locations.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the locations.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the locations.
 *         schema:
 *           type: integer
 */
router.get('/getlocations/:limit/:offset', location_controller.get_locations);

module.exports = router