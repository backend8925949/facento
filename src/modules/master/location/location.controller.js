const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_location = async (req, res, next) => {
    try {
        let location_details = await axios_instance.post('/api/rest/location/savelocation', { ascii_name: req.body.ascii_name, isd_code: req.body.isd_code, latitude: req.body.latitude, parent_id: req.body.parent_id, postal_code: req.body.postal_code, status: req.body.status, time_zone_id: req.body.time_zone_id, type: req.body.type, uuid: await helpers.GenerateUuid(), name: req.body.name, level: req.body.level, iso3: req.body.iso3, iso2: req.body.iso2, image: req.body.image, flag_icon: req.body.flag_icon, elevation: req.body.elevation, created_date: await helpers.GetDateTime() });
        let location = location_details.data.insert_facento_core_geo_location.returning;
        if (location) {
            return res.status(200).send({ location: location[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create location" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_location = async (req, res, next) => {
    try {
        let location_details = await axios_instance.put('/api/rest/location/updatelocation', { location_id: req.body.location_id, ascii_name: req.body.ascii_name, isd_code: req.body.isd_code, latitude: req.body.latitude, parent_id: req.body.parent_id, postal_code: req.body.postal_code, status: req.body.status, time_zone_id: req.body.time_zone_id, type: req.body.type, name: req.body.name, level: req.body.level, iso3: req.body.iso3, iso2: req.body.iso2, image: req.body.image, flag_icon: req.body.flag_icon, elevation: req.body.elevation, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let location = location_details.data.update_facento_core_geo_location.returning;
        if (location) {
            return res.status(200).send({ location: location[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid location details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_locations = async (req, res, next) => {
    try {
        let location_records = await axios_instance.post('/api/rest/location/getlocations', { limit: req.params.limit, offset: req.params.offset });
        let locations = location_records.data.facento_core_geo_location;
        if (locations) {
            return res.status(200).send({ records: locations });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_location_details = async (req, res, next) => {
    try {
        let location_details = await axios_instance.post('/api/rest/location/getlocation', { location_id: req.params.location_id });
        let location = location_details.data.facento_core_geo_location;
        if (location) {
            return res.status(200).send({ data: location });
        }
        else {
            return res.status(404).send({ message: "Location details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_location = async (req, res, next) => {
    try {
        const location_details = await get_location(req.body.location_id);
        console.log('location....', location_details);
        if (location_details) {
            let delete_location = await axios_instance.delete('/api/rest/location/deletelocation', { location_id: location_details.uuid });
            if (delete_location) {
                return res.status(200).send({ message: 'Location details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "Location details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_location = async (location_id) => {
    try {
        let location_details = await axios_instance.post('/api/rest/org/getlocation', { location_id: location_id });
        let location = location_details.data.facento_core_geo_location;
        if (location) {
            return location[0];
        }
        else {
            return { message: "Location details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_location,
    update_location,
    delete_location,
    get_locations,
    get_location_details,
}