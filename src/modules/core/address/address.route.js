const express = require('express');
const router = express.Router();


const address_controller = require('./address.controller');

router.post('/saveaddress', address_controller.save_address);
router.put('/updateaddress', address_controller.update_address);
router.delete('/deleteaddress', address_controller.delete_address);

/**
 * @swagger
 * /api/v1/address/getaddress/{address_id}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the details of a single account address.
 *     description: Retrieves the details of a single account address. Can be used to populate the details of the address.
 *     parameters:
 *       - in: path
 *         name: address_id
 *         required: true
 *         description: String uuid of the account address.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getaddress/:address_id', address_controller.get_address_details);

/**
 * @swagger
 * /api/v1/address/getaddresses/{limit}{offset}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the list of the addresses.
 *     description: Retrieves the list of the addresses.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the addresses.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the addresses.
 *         schema:
 *           type: integer
 */
router.get('/getaddresses/:limit/:offset', address_controller.get_addresss);

module.exports = router