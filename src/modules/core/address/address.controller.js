const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_address = async (req, res, next) => {
    try {
        let address_details = await axios_instance.post('/api/rest/address/saveaddress', { uuid: await helpers.GenerateUuid(), address_line1: req.body.address_line1, address_line2: req.body.address_line2, address_type: req.body.address_type, attention: req.body.attention, city_id: req.body.city_id, contact_type: req.body.contact_type, country_id: req.body.country_id, entity_id: req.body.entity_id, is_primary: req.body.is_primary, landmark: req.body.landmark, latitude: req.body.latitude, locality_id: req.body.locality_id, longitude: req.body.longitude, post_box_number: req.body.post_box_number, postal_code: req.body.postal_code, region_id: req.body.region_id, state_id: req.body.state_id, status: req.body.status, street_address: req.body.street_address, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let address = address_details.data.insert_facento_core_address.returning;
        if (address) {
            return res.status(200).send({ address: address[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create address" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_address = async (req, res, next) => {
    try {
        let address_details = await axios_instance.put('/api/rest/address/updateaddress', { address_id: req.body.address_id, address_line1: req.body.address_line1, address_line2: req.body.address_line2, address_type: req.body.address_type, attention: req.body.attention, city_id: req.body.city_id, contact_type: req.body.contact_type, country_id: req.body.country_id, entity_id: req.body.entity_id, is_primary: req.body.is_primary, landmark: req.body.landmark, latitude: req.body.latitude, locality_id: req.body.locality_id, longitude: req.body.longitude, post_box_number: req.body.post_box_number, postal_code: req.body.postal_code, region_id: req.body.region_id, state_id: req.body.state_id, status: req.body.status, street_address: req.body.street_address, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let address = address_details.data.update_facento_core_address.returning;
        if (address) {
            return res.status(200).send({ address: address[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid address details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_addresss = async (req, res, next) => {
    try {
        let address_records = await axios_instance.post('/api/rest/address/getaddresses', { limit: req.params.limit, offset: req.params.offset });
        let addresss = address_records.data.facento_core_address;
        if (addresss) {
            return res.status(200).send({ records: addresss });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_address_details = async (req, res, next) => {
    try {
        let address_details = await axios_instance.post('/api/rest/address/getaddress', { user_id: req.params.address_id });
        let address = address_details.data.facento_core_address;
        if (address) {
            return res.status(200).send({ data: address });
        }
        else {
            return res.status(404).send({ message: "address details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_address = async (req, res, next) => {
    try {
        const address_details = await get_address(req.body.address_id);
        console.log('address....', address_details);
        if (address_details) {
            let delete_address = await axios_instance.delete('/api/rest/address/deleteaddress', { address_id: address_details.uuid });
            if (delete_address) {
                return res.status(200).send({ message: 'address details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "address details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_address = async (address_id) => {
    try {
        let address_details = await axios_instance.post('/api/rest/address/getaddress', { address_id: address_id });
        let address = address_details.data.facento_core_address;
        if (address) {
            return address[0];
        }
        else {
            return { message: "address details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_address,
    update_address,
    delete_address,
    get_addresss,
    get_address_details,
}