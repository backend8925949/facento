const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const register_contact_person = async (req, res, next) => {
    try {
        let contact_person_details = await axios_instance.post('/api/rest/cp/savecontactperson', { uuid: await helpers.GenerateUuid(), work_location: req.body.work_location, twitter_url: req.body.twitter_url, status: req.body.status, organization_id: req.body.organization_id, mobile: req.body.mobile, middle_name: req.body.middle_name, logo: req.body.logo, first_name: req.body.first_name, account_type_id: req.body.account_type_id, auth_account_id: req.body.auth_account_id, display_name: req.body.display_name, email: req.body.email, facebook_url: req.body.facebook_url, family_name: req.body.family_name, image: req.body.image, job_title: req.body.job_title, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let contact_person = contact_person_details.data.insert_facento_core_contact_person.returning;
        if (contact_person) {
            return res.status(200).send({ contact_person: contact_person[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create contact_person" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_contact_person = async (req, res, next) => {
    try {
        let contact_person_details = await axios_instance.put('/api/rest/cp/updatecontactperson', { user_id: req.body.user_id, work_location: req.body.work_location, twitter_url: req.body.twitter_url, status: req.body.status, organization_id: req.body.organization_id, mobile: req.body.mobile, middle_name: req.body.middle_name, logo: req.body.logo, first_name: req.body.first_name, account_type_id: req.body.account_type_id, auth_account_id: req.body.auth_account_id, display_name: req.body.display_name, email: req.body.email, facebook_url: req.body.facebook_url, family_name: req.body.family_name, image: req.body.image, job_title: req.body.job_title, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let contact_person = contact_person_details.data.update_facento_core_contact_person.returning;
        if (contact_person) {
            return res.status(200).send({ contact_person: contact_person[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid contact_person details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_contact_persons = async (req, res, next) => {
    try {
        let contact_person_records = await axios_instance.post('/api/rest/cp/getcontactpersons', { limit: req.params.limit, offset: req.params.offset });
        let contact_persons = contact_person_records.data.facento_core_contact_person;
        if (contact_persons) {
            return res.status(200).send({ records: contact_persons });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_contact_person_details = async (req, res, next) => {
    try {
        let contact_person_details = await axios_instance.post('/api/rest/cp/getcontactperson', { user_id: req.params.user_id });
        let contact_person = contact_person_details.data.facento_core_contact_person;
        if (contact_person) {
            return res.status(200).send({ data: contact_person });
        }
        else {
            return res.status(404).send({ message: "contact_person details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_contact_person = async (req, res, next) => {
    try {
        const contact_person_details = await get_contact_person(req.body.user_id);
        console.log('contact_person....', contact_person_details);
        if (contact_person_details) {
            let delete_contact_person = await axios_instance.delete('/api/rest/cp/deletecontactperson', { user_id: contact_person_details.uuid });
            if (delete_contact_person) {
                return res.status(200).send({ message: 'contact_person details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "contact_person details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_contact_person = async (user_id) => {
    try {
        let contact_person_details = await axios_instance.post('/api/rest/cp/getcontactperson', { user_id: user_id });
        let contact_person = contact_person_details.data.facento_core_contact_person;
        if (contact_person) {
            return contact_person[0];
        }
        else {
            return { message: "contact_person details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    register_contact_person,
    update_contact_person,
    delete_contact_person,
    get_contact_persons,
    get_contact_person_details,
}