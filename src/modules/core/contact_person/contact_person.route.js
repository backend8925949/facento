const express = require('express');
const router = express.Router();


const contact_person_controller = require('./contact_person.controller');

router.post('/registercontactperson', contact_person_controller.register_contact_person);
router.put('/updatecontactperson', contact_person_controller.update_contact_person);
router.delete('/deletecontactperson', contact_person_controller.delete_contact_person);

/**
 * @swagger
 * /api/v1/contact/getcontact_person/{user_id}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the details of the contact person.
 *     description: Retrieves the details of the contact person. Can be used to populate the details of the contact person.
 *     parameters:
 *       - in: path
 *         name: user_id
 *         required: true
 *         description: String uuid of the contact person.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getcontact_person/:user_id', contact_person_controller.get_contact_person_details);

/**
 * @swagger
 * /api/v1/contact/getcontact_person/{limit}{offset}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the list of the contact persons.
 *     description: Retrieves the list of the contact persons.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the contact persons.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the contact persons.
 *         schema:
 *           type: integer
 */
router.get('/getcontact_persons/:limit/:offset', contact_person_controller.get_contact_persons);

module.exports = router