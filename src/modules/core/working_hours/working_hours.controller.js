const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_working_hours = async (req, res, next) => {
    try {
        let working_hours_details = await axios_instance.post('/api/rest/whrs/saveworkinghours', { uuid: await helpers.GenerateUuid(), organization_id: req.body.organization_id, valid_from: req.body.valid_from, valid_through: req.body.valid_through, opens: req.body.opens, closed: req.body.closed, break_start: req.body.break_start, break_end: req.body.break_end, status: req.body.status, day_of_week: req.body.day_of_week, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let working_hours = working_hours_details.data.insert_facento_core_working_hours.returning;
        if (working_hours) {
            return res.status(200).send({ working_hours: working_hours[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create working_hours" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_working_hours = async (req, res, next) => {
    try {
        let working_hours_details = await axios_instance.put('/api/rest/whrs/updateworkinghours', { working_hours_id: req.body.working_hours_id, organization_id: req.body.organization_id, valid_from: req.body.valid_from, valid_through: req.body.valid_through, opens: req.body.opens, closed: req.body.closed, break_start: req.body.break_start, break_end: req.body.break_end, status: req.body.status, day_of_week: req.body.day_of_week, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let working_hours = working_hours_details.data.update_facento_core_working_hours.returning;
        if (working_hours) {
            return res.status(200).send({ working_hours: working_hours[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid working_hours details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_working_hourss = async (req, res, next) => {
    try {
        let working_hours_records = await axios_instance.post('/api/rest/whrs/getworkinghours', { limit: req.params.limit, offset: req.params.offset });
        let working_hourss = working_hours_records.data.facento_core_working_hours;
        if (working_hourss) {
            return res.status(200).send({ records: working_hourss });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_working_hours_details = async (req, res, next) => {
    try {
        let working_hours_details = await axios_instance.post('/api/rest/whrs/getworkinghour', { working_hours_id: req.params.working_hours_id });
        let working_hours = working_hours_details.data.facento_core_working_hours;
        if (working_hours) {
            return res.status(200).send({ data: working_hours });
        }
        else {
            return res.status(404).send({ message: "working_hours details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_working_hours = async (req, res, next) => {
    try {
        const working_hours_details = await get_working_hours(req.body.working_hours_id);
        console.log('working_hours....', working_hours_details);
        if (working_hours_details) {
            let delete_working_hours = await axios_instance.delete('/api/rest/whrs/deleteworkinghours', { working_hours_id: working_hours_details.uuid });
            if (delete_working_hours) {
                return res.status(200).send({ message: 'working_hours details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "working_hours details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_working_hours = async (working_hours_id) => {
    try {
        let working_hours_details = await axios_instance.post('/api/rest/whrs/getworkinghour', { working_hours_id: working_hours_id });
        let working_hours = working_hours_details.data.facento_core_working_hours;
        if (working_hours) {
            return working_hours[0];
        }
        else {
            return { message: "working_hours details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_working_hours,
    update_working_hours,
    delete_working_hours,
    get_working_hourss,
    get_working_hours_details,
}