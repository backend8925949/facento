const express = require('express');
const router = express.Router();


const working_hours_controller = require('./working_hours.controller');

router.post('/registerworkinghours', working_hours_controller.save_working_hours);
router.put('/updateworkinghours', working_hours_controller.update_working_hours);
router.delete('/deleteworkinghours', working_hours_controller.delete_working_hours);

/**
 * @swagger
 * /api/v1/work/getworkinghours/{working_hours_id}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the details of the working hours.
 *     description: Retrieves the details of the working hours. Can be used to populate the details the working hours.
 *     parameters:
 *       - in: path
 *         name: working_hours_id
 *         required: true
 *         description: String uuid of the working hours.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getworkinghours/:working_hours_id', working_hours_controller.get_working_hours_details);

/**
 * @swagger
 * /api/v1/work/getworkinghours/{limit}{offset}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the list of the working hours.
 *     description: Retrieves the list of the working hours.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the working hours.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the working hours.
 *         schema:
 *           type: integer
 */
router.get('/getworkinghourss/:limit/:offset', working_hours_controller.get_working_hourss);

module.exports = router