const express = require('express');
const router = express.Router();


const organization_controller = require('./organization.controller');

router.post('/registerorganization', organization_controller.register_organization);
router.put('/updateorganization', organization_controller.update_organization);
router.delete('/deleteorganization', organization_controller.delete_organization);

/**
 * @swagger
 * /api/v1/organization/getorganization/{organization_id}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the details of a single organization.
 *     description: Retrieves the details of a single organization. Can be used to populate the details of the organization.
 *     parameters:
 *       - in: path
 *         name: organization_id
 *         required: true
 *         description: String uuid of the organization.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getorganization/:organization_id', organization_controller.get_organization_details);

/**
 * @swagger
 * /api/v1/organization/getorganization/{limit}{offset}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the list of organizations.
 *     description: Retrieves the list of organizations.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve organizations.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve organizations.
 *         schema:
 *           type: integer
 */
router.get('/getorganizations/:limit/:offset', organization_controller.get_organizations);

module.exports = router