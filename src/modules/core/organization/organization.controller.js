const { axios_instance } = require("../../../helpers/axios/axios-instance");
const helpers = require('../../../helpers/helper.function');

const register_organization = async (req, res, next) => {
    try {
        let organization_details = await axios_instance.post('/api/rest/org/registerorganization', { uuid: await helpers.GenerateUuid(), name: req.body.name, legal_name: req.body.business_name, display_name: req.body.business_nick_name, email: req.body.email, mobile: req.body.mobile_number, description: req.body.description, slogan: req.body.business_slogan, telephone: req.body.contact_number, fax_number: req.body.fax, contact_person_id: req.body.contact_person_id, account_id: req.body.account_id, time_zone_id: req.body.time_zone_id, status: req.body.status, created_by: req.body.owner_id, created_date: await helpers.GetDateTime(), type_id: req.body.business_type });
        let organization = organization_details.data.insert_facento_core_organization.returning;
        if (organization) {
            return res.status(200).send({ organization: organization[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create organization" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_organization = async (req, res, next) => {
    try {
        let organization_details = await axios_instance.put('/api/rest/org/updateorganizationdetails', { organization_id: req.body.organization_id, name: req.body.name, legal_name: req.body.business_name, display_name: req.body.business_nick_name, email: req.body.email, mobile: req.body.mobile_number, description: req.body.description, slogan: req.body.business_slogan, telephone: req.body.contact_number, fax_number: req.body.fax, contact_person_id: req.body.contact_person_id, account_id: req.body.account_id, time_zone_id: req.body.time_zone_id, status: req.body.status, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime(), type_id: req.body.business_type });
        let organization = organization_details.data.update_facento_core_organization.returning;
        if (organization) {
            return res.status(200).send({ organization: organization[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid organization details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_organizations = async (req, res, next) => {
    try {
        let organization_records = await axios_instance.post('/api/rest/org/getorganizations', { limit: req.params.limit, offset: req.params.offset });
        let organizations = organization_records.data.facento_core_organization;
        if (organizations) {
            return res.status(200).send({ records: organizations });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_organization_details = async (req, res, next) => {
    try {
        let organization_details = await axios_instance.post('/api/rest/org/getorganization', { user_id: req.params.organization_id });
        let organization = organization_details.data.facento_core_organization;
        if (organization) {
            return res.status(200).send({ data: organization });
        }
        else {
            return res.status(404).send({ message: "Organization details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_organization = async (req, res, next) => {
    try {
        const organization_details = await get_organization(req.body.organization_id);
        console.log('organization....', organization_details);
        if (organization_details) {
            let delete_creds = await axios_instance.delete('/api/rest/org/deleteorganization', { organization_id: organization_details.uuid });
            if (delete_creds) {
                return res.status(200).send({ message: 'Organization details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "Organization details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_organization = async (organization_id) => {
    try {
        let organization_details = await axios_instance.post('/api/rest/org/getorganization', { organization_id: organization_id });
        let organization = organization_details.data.facento_core_organization;
        if (organization) {
            return organization[0];
        }
        else {
            return { message: "User details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    register_organization,
    update_organization,
    delete_organization,
    get_organizations,
    get_organization_details,
}