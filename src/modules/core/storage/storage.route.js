const express = require('express');
const router = express.Router();


const storage_controller = require('./storage.controller');

router.post('/savestorage', storage_controller.save_storage);
router.put('/updatestorage', storage_controller.update_storage);
router.delete('/deletestorage', storage_controller.delete_storage);

/**
 * @swagger
 * /api/v1/storage/getstorage/{storage_id}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the details of the storage.
 *     description: Retrieves the details of the storage. Can be used to populate the details of the storage.
 *     parameters:
 *       - in: path
 *         name: storage_id
 *         required: true
 *         description: String uuid of the storage.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         ...
 */
router.get('/getstorage/:storage_id', storage_controller.get_storage_details);

/**
 * @swagger
 * /api/v1/storage/getstorage/{limit}{offset}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the list of storages.
 *     description: Retrieves the list of storages.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve storages.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve storages.
 *         schema:
 *           type: integer
 */
router.get('/getstorages/:limit/:offset', storage_controller.get_storages);

module.exports = router