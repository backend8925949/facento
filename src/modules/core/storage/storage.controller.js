const { axios_instance } = require("./../../../helpers/axios/axios-instance");
const helpers = require('./../../../helpers/helper.function');

const save_storage = async (req, res, next) => {
    try {
        let storage_details = await axios_instance.post('/api/rest/storage/savestorage', { uuid: req.body.storage_id, caption: req.body.caption, comment: req.body.comment, content_size: req.body.content_size, content_type: req.body.content_type, extention: req.body.extention, height: req.body.height, keywords: req.body.keywords, name: req.body.name, owner_acc_id: req.body.owner_acc_id, owner_uuid: req.body.owner_uuid, path: req.body.path, source: req.body.source, status: req.body.status, thumbnail_id: req.body.thumbnail_id, thumbnail_url: req.body.thumbnail_url, type: req.body.type, url: req.body.url, width: req.body.width, created_by: req.body.owner_id, created_date: await helpers.GetDateTime() });
        let storage = storage_details.data.insert_facento_core_storage.returning;
        if (storage) {
            return res.status(200).send({ storage: storage[0] });
        }
        else {
            return res.status(404).send({ message: "Unable to create storage" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const update_storage = async (req, res, next) => {
    try {
        let storage_details = await axios_instance.put('/api/rest/storage/updatestorage', { storage_id: req.body.storage_id, caption: req.body.caption, comment: req.body.comment, content_size: req.body.content_size, content_type: req.body.content_type, extention: req.body.extention, height: req.body.height, keywords: req.body.keywords, name: req.body.name, owner_acc_id: req.body.owner_acc_id, owner_uuid: req.body.owner_uuid, path: req.body.path, source: req.body.source, status: req.body.status, thumbnail_id: req.body.thumbnail_id, thumbnail_url: req.body.thumbnail_url, type: req.body.type, url: req.body.url, width: req.body.width, updated_by: req.body.owner_id, updated_date: await helpers.GetDateTime() });
        let storage = storage_details.data.update_facento_core_file_store.returning;
        if (storage) {
            return res.status(200).send({ storage: storage[0] });
        }
        else {
            return res.status(404).send({ message: "Invalid storage details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_storages = async (req, res, next) => {
    try {
        let storage_records = await axios_instance.post('/api/rest/storage/getstorages', { limit: req.params.limit, offset: req.params.offset });
        let storages = storage_records.data.facento_core_file_store;
        if (storages) {
            return res.status(200).send({ records: storages });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_storage_details = async (req, res, next) => {
    try {
        let storage_details = await axios_instance.post('/api/rest/storage/getstorage', { storage_id: req.params.storage_id });
        let storage = storage_details.data.facento_core_file_store;
        if (storage) {
            return res.status(200).send({ data: storage });
        }
        else {
            return res.status(404).send({ message: "storage details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const delete_storage = async (req, res, next) => {
    try {
        const storage_details = await get_storage(req.body.storage_id);
        console.log('storage....', storage_details);
        if (storage_details) {
            let delete_storage = await axios_instance.delete('/api/rest/storage/deletestorage', { storage_id: storage_details.uuid });
            if (delete_storage) {
                return res.status(200).send({ message: 'storage details removed successfully.' });
            }
        }
        else {
            return res.status(404).send({ message: "storage details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_storage = async (storage_id) => {
    try {
        let storage_details = await axios_instance.post('/api/rest/storage/getstorage', { storage_id: storage_id });
        let storage = storage_details.data.facento_core_file_store;
        if (storage) {
            return storage[0];
        }
        else {
            return { message: "storage details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

module.exports = {
    save_storage,
    update_storage,
    delete_storage,
    get_storages,
    get_storage_details,
}