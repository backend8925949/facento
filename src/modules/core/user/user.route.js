const express = require('express');
const router = express.Router();


const user_controller = require('./user.controller');

router.post('/registeruser', user_controller.register_user);
router.put('/updateuser', user_controller.update_user);
router.delete('/deleteuser', user_controller.delete_user);

/**
 * @swagger
 * /api/v1/user/getusers/{limit}{offset}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the list of the suers.
 *     description: Retrieves the list of the users registered on the system.
 *     parameters:
 *       - in: path
 *         name: limit
 *         required: true
 *         description: Numeric limit to retrieve the users.
 *         schema:
 *           type: integer
 *       - in: path
 *         name: offset
 *         required: true
 *         description: Numeric offset to retrieve the users.
 *         schema:
 *           type: integer
 */
router.get('/getusers/:limit/:offset', user_controller.get_users);

/**
 * @swagger
 * /api/v1/user/getuser/{user_id}:
 *   get:
 *     tags:
 *       - core
 *     summary: Retrieves the details of a single user.
 *     description: Retrieves the details of a single uswer. Can be used to populate the details of user.
 *     parameters:
 *       - in: path
 *         name: user_id
 *         required: true
 *         description: String uuid of the user.
 *         schema:
 *           type: string
 */
router.get('/getuser/:user_id', user_controller.get_user_details);

module.exports = router