const { axios_instance } = require("../../../helpers/axios/axios-instance");
const helpers = require('../../../helpers/helper.function');

const register_user = async (req, res, next) => {
    try {
        let AuthPin = await helpers.EncryptHash(req.body.AuthPin);
        let user_details = await axios_instance.post('/api/rest/user/registeruser', { account_type: req.body.account_type, organization_id: req.body.organization_id, first_name: req.body.first_name, middle_name: req.body.middle_name, last_name: req.body.last_name, display_name: req.body.display_name, email: req.body.email, mobile: req.body.mobile, description: req.body.description, registration_source: req.body.registration_source, is_system_account: req.body.is_system_account, role: req.body.role, country_id: req.body.country_id, time_zone_id: req.body.time_zone_id, parent_account_id: req.body.parent_account_id, is_invite_sent: req.body.is_invite_sent, status: req.body.status, created_by: req.body.created_by, created_date: await helpers.GetDateTime(), uuid: await helpers.GenerateUuid(), unhashed_pin: req.body.AuthPin, reporting_id: req.body.reporting_id, pin: AuthPin, poster_id: req.body.poster_id, referral_id: req.body.referral_id, account_id: req.body.account_id, claimed_time:  await helpers.GetDateTime(), icon_id: req.body.icon_id });
        let user = user_details.data.insert_facento_core_auth_account.returning;
        if (user) {
            let user_credentials = await axios_instance.post('/api/rest/user/saveusercreds', { email: req.body.email, status: req.body.status, created_by: user[0].id, created_date:  await helpers.GetDateTime(), auth_type: 1, password: req.body.password, is_change_on_access: false, is_multi_factor: false, valid_from: await helpers.GetDateTime(), valid_until: req.body.valid_until, auth_account_id: user[0].id, is_primary: true, last_accessed_on:  await helpers.GetDateTime() });
            let user_creds = user_credentials.data.insert_facento_core_auth_account_creds.returning;
            if (user_creds) {
                return res.status(200).send({ user: user[0], user_auth: user_creds[0] });
            }
            else {
                return res.status(404).send({ message: "Unable to create user credentials" });
            }
        }
        else {
            return res.status(404).send({ message: "Unable to create user" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}


const update_user = async (req, res, next) => {
    try {
        let user_details = await axios_instance.put('/api/rest/user/updateuserdetails', { user_id: req.body.user_id, user_id: req.body.user_id, account_type: req.body.account_type, organization_id: req.body.organization_id, first_name: req.body.first_name, middle_name: req.body.middle_name, last_name: req.body.last_name, display_name: req.body.display_name, email: req.body.email, mobile: req.body.mobile, description: req.body.description, referral_Number: req.body.referral_Number, registration_source: req.body.registration_source, is_system_account: req.body.is_system_account, role: req.body.role, country_id: req.body.country_id, time_zone_id: req.body.time_zone_id, parent_account_id: req.body.parent_account_id, is_invite_sent: req.body.is_invite_sent, status: req.body.status, updated_by: req.body.updated_by, updated_date:  await helpers.GetDateTime(), unhashed_pin: req.body.unhashed_pin, reporting_id: req.body.reporting_id, pin: req.body.pin, poster_id: req.body.poster_id, referral_id: req.body.referral_id, account_id: req.body.account_id, claimed_time: await helpers.GetDateTime(), icon_id: req.body.icon_id });
        let user = user_details.data.update_facento_core_auth_account.returning;
        if (user) {
            let user_credentials = await axios_instance.put('/api/rest/user/updateusercreds', { email: req.body.email, status: req.body.status, updated_by: user[0].id, updated_date:  await helpers.GetDateTime(), auth_type: req.body.auth_type, password: req.body.middle_name, is_change_on_access: req.body.is_change_on_access, is_multi_factor: req.body.is_multi_factor, valid_from: req.body.valid_from, valid_until: req.body.valid_until, auth_account_id: user[0].id, is_primary: req.body.is_primary, last_accessed_on:  await helpers.GetDateTime() });
            let user_creds = user_credentials.data.update_facento_core_auth_account_creds.returning;
            if (user_creds) {
                return res.status(200).send({ user: user[0], user_auth: user_creds[0] });
            }
            else {
                return res.status(404).send({ message: "No user credentials found to update" });
            }
        }
        else {
            return res.status(404).send({ message: "Invalid user details" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

const get_users = async (req, res, next) => {
    try {
        let user_records = await axios_instance.post('/api/rest/user/getusers', { limit: req.params.limit, offset: req.params.offset });
        let users = user_records.data.facento_core_auth_account;
        if (users) {
            return res.status(200).send({ records: users });
        }
        else {
            return res.status(404).send({ message: "Currently no records available" });
        }
    } catch (err) {
        console.log('error', err.message);
        res.status(500).send({ message: err.message });
    };
}

const get_user_details = async (req, res, next) => {
    try {
        let user_details = await axios_instance.post('/api/rest/user/getuserdetails', { user_id: req.params.user_id });
        let user = user_details.data.facento_core_auth_account;
        if (user) {
            return res.status(200).send({ data: user });
        }
        else {
            return res.status(404).send({ message: "User details not found" });
        }
    } catch (err) {
        console.log('error', err.message);
        return res.status(500).send({ message: err.message });
    };
}

const get_user = async (email) => {
    try {
        let user_details = await axios_instance.post('/api/rest/user/getuser', { email: email });
        let user = user_details.data.facento_core_auth_account;
        if (user) {
            return user[0];
        }
        else {
            return { message: "User details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

const get_user_creds = async (auth_account_id, email, password) => {
    try {
        let user_credentials = await axios_instance.post('/api/rest/user/getcreds', { auth_account_id: auth_account_id, user_name: email, password: password });
        let user_creds = user_credentials.data.facento_core_auth_account_creds;
        if (user_creds) {
            return user_creds[0];
        }
        else {
            return { message: "User details not found" };
        }
    } catch (err) {
        console.log('error', err.message);
        return err.message;
    }
}

const delete_user = async (req, res, next) => {
    try {
        const user_details = await get_user(req.body.email);
        console.log('user....', user_details);
        if (user_details) {
            let delete_creds = await axios_instance.delete('/api/rest/user/deleteusercreds', { auth_account_id: user_details.id });
            if (delete_creds) {
                let delete_details = await axios_instance.delete('/api/rest/user/deleteuserdetails', { user_id: user_details.uuid });
                if (delete_details) {
                    return res.status(200).send({ message: 'User details removed successfully.' });
                }
            }
        }
        else {
            return res.status(404).send({ message: "User details not found" });
        }
    } catch (err) {
        console.log('error', err);
        return res.status(500).send({ message: err.message });
    }
}

module.exports = {
    get_users,
    get_user,
    register_user,
    get_user_creds,
    get_user_details,
    update_user,
    delete_user,
}