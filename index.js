// packages import
const express = require('express');
const app = express();
const cors = require('cors');
const axios = require('axios');
app.use(cors());
app.use(express.json());

const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerFile = require('./swagger_output.json');
const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
      title: 'Facento APIs',
      version: '1.0.0',
    },
    host: 'localhost:8086',
    basePath:'/api/v1',
    schemes: ['http','https'],
    components:
    {
      securitySchemes:
      {
        bearerAuth:
        {
        type: 'http',
        scheme: 'bearer',
        // bearerFormat: `JWT  # optional, for documentation purposes only`
        bearerFormat: 'JWT',
        name: 'Authorization',
        in:'header'
        }
      },
    },

    security:
      [{bearerAuth: []}]
  };
  
// add swagger doc
const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: [
        './src/routes/*.js',
        './src/modules/authentication/auth.route.js',
        './src/modules/core/user/user.route.js',
        './src/modules/core/organization/organization.route.js',
        './src/modules/core/address/address.route.js',
        './src/modules/core/contact_person/contact_person.route.js',
        './src/modules/core/storage/storage.route.js',
        './src/modules/core/working_hours/working_hours.route.js',
        './src/modules/master/location/location.route.js',
        './src/modules/master/currency/currency.route.js',
        './src/modules/master/timezone/timezone.route.js',
        './src/modules/account_tax/acc_tax_authority/acc_tax_authority.route.js',
        './src/modules/account_tax/acc_tax/acc_tax.route.js',
        './src/modules/account_tax/acc_tax_group/acc_tax_group.route.js',
        './src/modules/account_tax/acc_tax_group_map/acc_tax_group_map.route.js',
    ],
  };
const swaggerSpec = swaggerJSDoc(options);
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile, {
    customCss: `
    .swagger-ui .topbar { display: none; }
    .opblock-body {display: flex; flex-direction: row;flex-wrap:wrap;}
    .opblock-body > div:first-child {
        flex: 1 0 100%;
      }
    .opblock-description-wrapper {width: 100% !important;}
    .responses-wrapper {flex-basis: 1;}
    button {
        max-height: 36px;
    }
    `
}));

//routers
const Router = require('./src/routes/routes');
app.use('/api/v1',Router)
const port = process.env.PORT || 8086;

// console text when app is running
app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`);
});

// to develop swagger api doc, run: npm run swagger-autogen