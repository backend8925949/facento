const swaggerAutogen = require('swagger-autogen')({openapi: '3.0.0'})
// const swaggerAutogen = require('swagger-autogen')

const doc = {
    info: {
      title: 'Facento Core API',
      description: 'Facento Core API v0.0.1',
    },
    host: 'localhost:8086',
    basePath:'/api/v1',
    schemes: ['http','https'],
    components:
    {
      securitySchemes:
      {
        bearerAuth:
        {
        type: 'http',
        scheme: 'bearer',
        // bearerFormat: `JWT  # optional, for documentation purposes only`
        bearerFormat: 'JWT',
        name: 'Authorization',
        in:'header'
        }
      },
    },

    security:
      [{bearerAuth: []}]
  };

const outputFile = './swagger_output.json'
const endpointsFiles = ['./src/routes/routes.js']

swaggerAutogen(outputFile, endpointsFiles, doc)